//
//  AnalyticsViewModel.swift
//  features
//
//  Created by ang on 2021/4/8.
//

import Foundation
import FirebaseAnalytics

class AnalyticsViewModel {
    
    var events: [String] = ["share", "login", "sign_up", "tutorial_begin", "tutorial_complete", "search", "select_content", "custom_sold_out", "view_item_list", "select_item", "view_item", "add_to_wishlist", "add_to_cart", "view_cart", "remove_from_cart", "begin_checkout", "add_shipping_info", "add_payment_info", "purchase", "refund", "view_promotion", "select_promotion"]
    var testData: [String] = [String](repeating: "TEST_Data", count: 999)
    
    // A pair of jeggings
    var jeggings: [String: Any] = [
        AnalyticsParameterItemID: "SKU_122",
        AnalyticsParameterItemName: "jeggings",
        AnalyticsParameterItemCategory: "pants",
        AnalyticsParameterItemVariant: "black",
        AnalyticsParameterItemBrand: "Google",
        AnalyticsParameterCurrency: "TWD",
        AnalyticsParameterPrice: 1.0,
    ]
    
    // A pair of boots
    var boots: [String: Any] = [
        AnalyticsParameterItemID: "SKU_456",
        AnalyticsParameterItemName: "boots",
        AnalyticsParameterItemCategory: "shoes",
        AnalyticsParameterItemVariant: "brown",
        AnalyticsParameterItemBrand: "Google"
//        ,AnalyticsParameterPrice: 24.99,
    ]
    
    // A pair of socks
    var socks: [String: Any] = [
        AnalyticsParameterItemID: "SKU_789",
        AnalyticsParameterItemName: "ankle_socks",
        AnalyticsParameterItemCategory: "socks",
        AnalyticsParameterItemVariant: "red",
        AnalyticsParameterItemBrand: "Google"
//        ,AnalyticsParameterPrice: 5.99,
    ]
    
    // Prepare ecommerce parameters
    var itemList: [String: Any] = [
        AnalyticsParameterItemListID: "L001",
        AnalyticsParameterItemListName: "Related products",
    ]
    
    // Prepare ecommerce parameters
    var selectedItem: [String: Any] = [
        AnalyticsParameterItemListID: "L001",
        AnalyticsParameterItemListName: "Related products",
    ]
    
    // Prepare ecommerce parameters
    var productDetails: [String: Any] = [
        AnalyticsParameterCurrency: "USD",
        AnalyticsParameterValue: 9.99
    ]
    
    // Prepare item detail params
    var itemDetails: [String: Any] = [
        AnalyticsParameterCurrency: "USD",
        AnalyticsParameterValue: 19.98
    ]
    
    // Prepare order parameters
    var orderParameters: [String: Any] = [
        AnalyticsParameterCurrency: "USD",
        AnalyticsParameterValue: 44.97
    ]
    
    // Prepare params
    var removeParams: [String: Any] = [
        AnalyticsParameterCurrency: "USD",
        AnalyticsParameterValue: 24.99
    ]
    
    // Prepare checkout params
    var checkoutParams: [String: Any] = [
        AnalyticsParameterCurrency: "USD",
        AnalyticsParameterValue: 14.98,
        AnalyticsParameterCoupon: "SUMMER_FUN"
    ]
    
    // Prepare shipping params
    var shippingParams: [String: Any] = [
        AnalyticsParameterCurrency: "USD",
        AnalyticsParameterValue: 14.98,
        AnalyticsParameterCoupon: "SUMMER_FUN",
        AnalyticsParameterShippingTier: "Ground"
    ]
    
    // Prepare payment params
    var paymentParams: [String: Any] = [
        AnalyticsParameterCurrency: "USD",
        AnalyticsParameterValue: 14.98,
        AnalyticsParameterCoupon: "SUMMER_FUN",
        AnalyticsParameterPaymentType: "Visa"
    ]
    
    // Prepare purchase params
    var purchaseParams: [String: Any] = [
        AnalyticsParameterTransactionID: "T12345",
        AnalyticsParameterAffiliation: "Google Store",
//        AnalyticsParameterCurrency: "USD",
        AnalyticsParameterCurrency: "TWD",
        AnalyticsParameterValue: 0.0,
        AnalyticsParameterPrice: 1,
        AnalyticsParameterTax: 0.0,
        AnalyticsParameterShipping: 0.0,
        AnalyticsParameterCoupon: "SUMMER_FUN"
    ]
    
    // Prepare refund params
    var refundParams: [String: Any] = [
        AnalyticsParameterTransactionID: "T12345",
        AnalyticsParameterCurrency: "USD",
        AnalyticsParameterValue: 9.99,
    ]
    
    // (Optional) for partial refunds, define the item ID and quantity of refunded items
    let refundedProduct: [String: Any] = [
        AnalyticsParameterItemID: "SKU_123",
        AnalyticsParameterQuantity: 1,
    ]
    
    // Prepare promotion parameters
    var promoParams: [String: Any] = [
        AnalyticsParameterPromotionID: "T12345",
        AnalyticsParameterPromotionName:"Summer Sale",
        AnalyticsParameterCreativeName: "summer2020_promo.jpg",
        AnalyticsParameterCreativeSlot: "featured_app_1",
        AnalyticsParameterLocationID: "HERO_BANNER",
    ]
    
    
}
