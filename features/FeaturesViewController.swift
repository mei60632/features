//
//  FeaturesViewController.swift
//  features
//
//  Created by ang on 2021/4/8.
//

import UIKit

class FeaturesViewController: UIViewController {
    
    @IBOutlet weak var mTableView: UITableView!
    
    private let cellName: String = "cell"
    private var viewModel: FeaturesViewModel = FeaturesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellName)
        
    }
    
}

extension FeaturesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.features.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellName)!
        cell.textLabel?.text = viewModel.features[indexPath.row]
        return cell
    }
    
}

extension FeaturesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var vc: UIViewController = UIViewController()
        switch indexPath.row {
        case 0:
            vc = AnalyticsViewController()
        case 1:
            fatalError()
        default:
            break
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Firebase"
    }
}
