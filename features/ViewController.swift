//
//  ViewController.swift
//  features
//
//  Created by ang on 2021/4/1.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Wellcome to iOS Team"

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func crash(_ sender: Any) {
        fatalError()
    }
    
    @IBAction func clickFeatures(_ sender: Any) {
        self.navigationController?.pushViewController(FeaturesViewController(), animated: true)
    }
    
    
}
