//
//  FeaturesViewModel.swift
//  features
//
//  Created by ang on 2021/4/8.
//

import Foundation

class FeaturesViewModel {
    
    var features: [String] = ["Firebase-Analytics", "Firebase-Crashily"]
    var testDatas: [String] = [String](repeating: "TestData_", count: 99999)
    
}
