//
//  AnalyticsViewController.swift
//  features
//
//  Created by ang on 2021/4/8.
//

import UIKit
import FirebaseAnalytics

class AnalyticsViewController: UIViewController {
    
    @IBOutlet weak var mTableView: UITableView!
    
    private let itemID: String = "0005"
    private let itemName: String = "花東清水斷崖、公正包子"
    private let contentType: String = "tour_promote"
    private let content: String = "內容內容內容內容內容內容內容內容內容"
    private let contentName: String = "花蓮一日遊"
    private let searchTerm: String = "搜尋條件"
    
    private let viewModel: AnalyticsViewModel = AnalyticsViewModel()
    private let cellName: String = "cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Analytics"
        mTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellName)
        // Do any additional setup after loading the view.
    }
    
}

extension AnalyticsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellName)!
        cell.textLabel?.text = viewModel.events[indexPath.row]
        return cell
    }
    
}

extension AnalyticsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch viewModel.events[indexPath.row] {
        case "share":
            Analytics.logEvent(AnalyticsEventShare, parameters: [
                AnalyticsParameterItemID: "0001",
                AnalyticsParameterContentType: contentType
            ])
        case "login":
            Analytics.setUserID("25115249-A1DE-4965-B40A-7DF224036D7A")
            Analytics.logEvent(AnalyticsEventLogin, parameters: [
                AnalyticsParameterMethod: "InMobi"
            ])
        case "sign_up":
            Analytics.logEvent(AnalyticsEventSignUp, parameters: [
                AnalyticsParameterMethod: "Apple sign"
            ])
        case "tutorial_begin":
            Analytics.logEvent(AnalyticsEventTutorialBegin, parameters: nil)
        case "tutorial_complete":
            Analytics.logEvent(AnalyticsEventTutorialComplete, parameters: nil)
        case "search":
            Analytics.logEvent(AnalyticsEventSearch, parameters: [
                AnalyticsParameterSearchTerm: searchTerm
            ])
        case "select_content":
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemID: itemID,
                AnalyticsParameterItemName: itemName,
                AnalyticsParameterContent: content,
                AnalyticsParameterContentType: contentType
            ])
        case "custom_sold_out":
            Analytics.logEvent("custom_sold_out", parameters: [
                AnalyticsParameterItemID: itemID,
                AnalyticsParameterItemName: itemName
            ])
        case "view_item_list":
            // Add item indexes
            viewModel.jeggings[AnalyticsParameterIndex] = 1
            viewModel.boots[AnalyticsParameterIndex] = 2
            viewModel.socks[AnalyticsParameterIndex] = 3
            // Add items
            viewModel.itemList[AnalyticsParameterItems] = [viewModel.jeggings, viewModel.boots, viewModel.socks]
            
            // Log view item list event
            Analytics.logEvent(AnalyticsEventViewItemList, parameters: viewModel.itemList)
        case "select_item":
            // Add items array
            viewModel.selectedItem[AnalyticsParameterItems] = [viewModel.jeggings]

            // Log select item event
            Analytics.logEvent(AnalyticsEventSelectItem, parameters: viewModel.selectedItem)
        case "view_item":
            // Add items array
            viewModel.productDetails[AnalyticsParameterItems] = [viewModel.jeggings]

            // Log view item event
            Analytics.logEvent(AnalyticsEventViewItem, parameters: viewModel.productDetails)
        case "add_to_wishlist":
            viewModel.jeggings[AnalyticsParameterQuantity] = 2
            // Add items
            viewModel.itemDetails[AnalyticsParameterItems] = [viewModel.jeggings]
            // Log an event when product is added to wishlist
            Analytics.logEvent(AnalyticsEventAddToWishlist, parameters: viewModel.itemDetails)
        case "add_to_cart":
            // Log an event when product is added to cart
            Analytics.logEvent(AnalyticsEventAddToCart, parameters: viewModel.itemDetails)
        case "view_cart":
            // Specify order quantity
            viewModel.jeggings[AnalyticsParameterQuantity] = 2
            viewModel.boots[AnalyticsParameterQuantity] = 1
            // Add items array
            viewModel.orderParameters[AnalyticsParameterItems] = [viewModel.jeggings, viewModel.boots]

            // Log event when cart is viewed
            Analytics.logEvent(AnalyticsEventViewCart, parameters: viewModel.orderParameters)
        case "remove_from_cart":
            // Specify removal quantity
            viewModel.boots[AnalyticsParameterQuantity] = 1
            // Add items
            viewModel.removeParams[AnalyticsParameterItems] = [viewModel.boots]

            // Log removal event
            Analytics.logEvent(AnalyticsEventRemoveFromCart, parameters: viewModel.removeParams)
        case "begin_checkout":
            // Add items
            viewModel.checkoutParams[AnalyticsParameterItems] = [viewModel.jeggings]

            // Log checkout event
            Analytics.logEvent(AnalyticsEventBeginCheckout, parameters: viewModel.checkoutParams)
        case "add_shipping_info":
            // Add items
            viewModel.shippingParams[AnalyticsParameterItems] = [viewModel.jeggings]

            // Log added shipping info event
            Analytics.logEvent(AnalyticsEventAddShippingInfo, parameters: viewModel.shippingParams)
        case "add_payment_info":
            // Add items
            viewModel.paymentParams[AnalyticsParameterItems] = [viewModel.jeggings]

            // Log added payment info event
            Analytics.logEvent(AnalyticsEventAddPaymentInfo, parameters: viewModel.paymentParams)
        case "purchase":
            // Add items
            viewModel.purchaseParams[AnalyticsParameterItems] = [viewModel.jeggings]

            // Log purchase event
            Analytics.logEvent(AnalyticsEventPurchase, parameters: viewModel.purchaseParams)
        case "refund":
            // Add items
            viewModel.refundParams[AnalyticsParameterItems] = [viewModel.refundedProduct]

            // Log refund event
            Analytics.logEvent(AnalyticsEventRefund, parameters: viewModel.refundParams)
        case "view_promotion":
            // Add items
            viewModel.promoParams[AnalyticsParameterItems] = [viewModel.jeggings]

            // Log event when promotion is displayed
            Analytics.logEvent(AnalyticsEventViewPromotion, parameters: viewModel.promoParams)
        case "select_promotion":
            // Add items
            viewModel.promoParams[AnalyticsParameterItems] = [viewModel.jeggings]
            // Log event when promotion is selected
            Analytics.logEvent(AnalyticsEventSelectPromotion, parameters: viewModel.promoParams)
        default:
            break
        }
    }
}
