//
//  AppDelegate.swift
//  features
//
//  Created by ang on 2021/3/24.
//

import UIKit
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate, UIWindowSceneDelegate {
    
    var mWindow: UIWindow = UIWindow()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        mWindow.makeKeyAndVisible()
        mWindow.rootViewController = UINavigationController(rootViewController: ViewController())
        FirebaseApp.configure()
        return true
    }
    
    
}

